import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CheckboxSelectionComponent } from './chbox-selection-demo/chbox-selection-demo.component';
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { PanelComponent } from './panel/panel.component';
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    CheckboxSelectionComponent,
    PanelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxDatatableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
