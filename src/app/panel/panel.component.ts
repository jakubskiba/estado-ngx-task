import {Component, OnInit, Input, Pipe} from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {

  filteredItems: TreeMenuElement[];
  treeMenuOptions: TreeMenuElement[];

  @Input() selected;
  constructor() {
    this.treeMenuOptions = [
      {
        name: "Edit",
        children: [
          {
            name: "Component",
            children: [
              {name: "Estado", children: []}
            ]
          },
          {
            name: "Status",
            children: [
              {name: "New", children: []},
              {name: "Assigned", children: []},
              {name: "Accepted", children: []},
              {name: "Fixed", children: []},
              {name: "Fixed (Verified)", children: []}
            ]
          },
          {
            name: "Priority",
            children: [

            ]
          },
          {
            name: "Severity",
            children: [

            ]
          },
          {
            name: "Iteration",
            children: [

            ]
          },
          {
            name: "Goals",
            children: [

            ]
          },
          {
            name: "Owner",
            children: [

            ]
          },
          {
            name: "Verifier",
            children: [

            ]
          },
          {
            name: "Tags",
            children: [

            ]
          }


        ]
      }
    ];
  }

  ngOnInit() {
  }

  filterElements(children: TreeMenuElement[], name) {
    console.log(children);
    console.log(name);
    if(!name || name == '') {
      this.filteredItems = children;
    }
    else {
      this.filteredItems = children.filter(element => element.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
    }
  }

}

class TreeMenuElement {
  name: string;
  children: TreeMenuElement[];
}
