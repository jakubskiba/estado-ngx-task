import { Component } from '@angular/core';
import {PanelComponent} from "../panel/panel.component";

@Component({
  selector: 'chkbox-selection-demo',
  templateUrl: '../chbox-selection-demo/chbox-selection-demo.template.html'
})
export class CheckboxSelectionComponent {

  rows = [];
  selected = [];

  constructor() {

    this.fetch((data) => {
      this.rows = data;
    });
  }

  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `https://raw.githubusercontent.com/swimlane/ngx-datatable/master/assets/data/company.json`);

    req.onload = () => {
      console.log(req.response);
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  onActivate(event) {
    console.log('Activate Event', event);
  }

  // add() {
  //   this.selected.push(this.rows[1], this.rows[3]);
  // }
  //
  // update() {
  //   this.selected = [ this.rows[1], this.rows[3] ];
  // }
  //
  // remove() {
  //   this.selected = [];
  // }

  displayCheck(row) {
    return row.name !== 'Ethel Price';
  }
}
